<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class TypeProduct extends Model
{
    protected $table = 'type_products';
    protected $guarded = [''];

    public function products() {

        return $this->belongsToMany(Product::class);
    }
}
