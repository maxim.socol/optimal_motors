<?php

namespace App\Http\Controllers;

use App\Models\Config;
use App\Models\Page;
use App\Models\Slider;
use App\Models\Contact;
use App\Models\Benefit;
use App\Models\CategoryProduct;
use App\Models\Product;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index() {

        $content = Page::where('slug', 'index')->first();
        $configs = Config::get();
        $sliders = Slider::orderBy('order', 'asc')->where('status', 1)->get();
        $benefits = Benefit::orderBy('order', 'asc')->where('status', 1)->get();
        $products = Product::where('show_on_home', 1)->take(3)->get();
        $categories = CategoryProduct::where('status', 1)->get();
        $popular_categories = CategoryProduct::orderByRaw("RAND()")->where('status', '1')->take(6)->get();

        return view('index', compact('configs', 'content', 'sliders', 'benefits', 'products', 'categories', 'popular_categories'));
    }

    public function about() {

        $content = Page::where('slug', 'despre-noi')->first();
        $categories = CategoryProduct::where('status', 1)->get();

        return view('about', compact('content', 'categories'));
    }

    public function contact() {

        $content = Page::where('slug', 'contact')->first();
        $categories = CategoryProduct::where('status', 1)->get();

        return view('contact', compact('content', 'categories'));
    }

    public function faq() {

        $content = Page::where('slug', 'faq')->first();
        $categories = CategoryProduct::where('status', 1)->get();

        return view('faq', compact('content', 'categories'));
    }

    public function terms() {
        
        $content = Page::where('slug', 'terms')->first();
        $categories = CategoryProduct::where('status', 1)->get();

        return view('terms', compact('content', 'categories'));
    }

    public function ajaxSendContacts(Request $request) {

        if (!$request->ajax())
            abort('404');

        $contacts = new Contact();

        if ($contacts->create($request->all())) {
            return response(['message' => 'Solicitarea de contact a fost trimisă cu succes', 'status' => 'success']);
        }

        return response(['message' => 'Error, something went wrong', 'status' => 'error']);
    }

}
