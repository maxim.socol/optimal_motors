<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CategoryProduct;
use App\Models\ColorProduct;
use App\Models\BrandProduct;
use App\Models\TypeProduct;

class Product extends Model
{
    protected $table = 'products';
    protected $guarded = [''];

    public function categories()
    {
        return $this->belongsTo(CategoryProduct::class, 'category_id');
    }

    public function colors()
    {
        return $this->belongsToMany(ColorProduct::class);
    }

    public function brands()
    {
        return $this->belongsTo(BrandProduct::class, 'brand_id');
    }

    public function types()
    {
        return $this->belongsToMany(TypeProduct::class);
    }
}
