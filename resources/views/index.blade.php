@extends('layouts.app', ['content' => $content])

@section('content')

    <div class="site__body">

        <!-- .block-slideshow -->
        @include('components.slider')
        <!-- .block-slideshow / end -->

        <!-- .block-features -->
        @include('components.features')
        <!-- .block-features / end -->

        <!-- .block-products-carousel -->
        @include('components.products_slider')
        <!-- .block-products-carousel / end -->

        <!-- .block-banner -->
        <div class="block block-banner">
            <div class="container">
                <a href="{{ url('products')}} " class="block-banner__body">
                    <div class="block-banner__image block-banner__image--desktop" style="background-image: url({{asset('assets/images/banners/banner-1.jpg')}})"></div>
                    <div class="block-banner__image block-banner__image--mobile" style="background-image: url({{asset('assets/images/banners/banner-1-mobile.jpg')}})"></div>
                    <div class="block-banner__title">{{ $configurations->get('name-site') }}
                        <br class="block-banner__mobile-br">{{ $configurations->get('partners') }}</div>
                    <div class="block-banner__text">{{ $configurations->get('partners-list') }}</div>
                    <div class="block-banner__button">
                        <span class="btn btn-sm btn-primary">{{ $configurations->get('partners-button-text') }}</span>
                    </div>
                </a>
            </div>
        </div>
        <!-- .block-banner / end -->
        <!-- .block-products -->
        @include('components.bestsellers')
        <!-- .block-products / end -->

        <!-- .block-categories -->
        @include('components.popular_categories')
        <!-- .block-categories / end -->

        <!-- .block-products-carousel -->
        @include('components.arrivals_products')
        <!-- .block-products-carousel / end -->

        {{-- <!-- .block-posts -->
        @include('components.news')
        <!-- .block-posts / end --> --}}

        {{-- <!-- .block-brands -->
        @include('components.brands')
        <!-- .block-brands / end --> --}}

        <!-- .block-product-columns -->
        @include('components.columns_product')
        <!-- .block-product-columns / end -->
    </div>
    
@endsection