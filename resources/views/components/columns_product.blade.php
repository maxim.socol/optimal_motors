<div class="block block-product-columns d-lg-block d-none">
    <div class="container">
        <div class="row">
            <div class="col-4">
                <div class="block-header">
                    <h3 class="block-header__title">{{ $configurations->get('produse-de-top') }}</h3>
                    <div class="block-header__divider"></div>
                </div>
                <div class="block-product-columns__column">
                    <div class="block-product-columns__item">
                        <div class="product-card product-card--layout--horizontal">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__badges-list">
                                <div class="product-card__badge product-card__badge--new">New</div>
                            </div>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-1.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Electric Planer Brandix KL370090G 300 Watts</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$749.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#compare-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-product-columns__item">
                        <div class="product-card product-card--layout--horizontal">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__badges-list">
                                <div class="product-card__badge product-card__badge--hot">Hot</div>
                            </div>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-2.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Undefined Tool IRadix DPS3000SY 2700 Watts</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$1,019.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#compare-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-product-columns__item">
                        <div class="product-card product-card--layout--horizontal">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-3.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Drill Screwdriver Brandix ALX7054 200 Watts</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$850.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#compare-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="block-header">
                    <h3 class="block-header__title">{{ $configurations->get('oferte-speciale') }}</h3>
                    <div class="block-header__divider"></div>
                </div>
                <div class="block-product-columns__column">
                    <div class="block-product-columns__item">
                        <div class="product-card product-card--layout--horizontal">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__badges-list">
                                <div class="product-card__badge product-card__badge--sale">Sale</div>
                            </div>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-4.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Drill Series 3 Brandix KSR4590PQS 1500 Watts</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices"><span class="product-card__new-price">$949.00</span> <span class="product-card__old-price">$1189.00</span></div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#compare-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-product-columns__item">
                        <div class="product-card product-card--layout--horizontal">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-5.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Brandix Router Power Tool 2017ERXPK</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$1,700.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#compare-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-product-columns__item">
                        <div class="product-card product-card--layout--horizontal">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-6.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Brandix Drilling Machine DM2019KW4 4kW</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$3,199.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#compare-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="block-header">
                    <h3 class="block-header__title">{{ $configurations->get('cele-mai-vandute') }}</h3>
                    <div class="block-header__divider"></div>
                </div>
                <div class="block-product-columns__column">
                    <div class="block-product-columns__item">
                        <div class="product-card product-card--layout--horizontal">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-7.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Brandix Pliers</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$24.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#compare-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-product-columns__item">
                        <div class="product-card product-card--layout--horizontal">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-8.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Water Hose 40cm</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$15.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#compare-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-product-columns__item">
                        <div class="product-card product-card--layout--horizontal">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-9.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Spanner Wrench</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$19.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#compare-16') }}"></use>
                                        </svg> <span class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>