<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class BrandProduct extends Model
{
    protected $table = 'brand_products';
    protected $with = ['products'];

    public function products()
    {
        return $this->hasMany(Product::class, 'brand_id');
    }
}
