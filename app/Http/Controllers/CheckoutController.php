<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;

class CheckoutController extends Controller
{
    public function ckeckout() {

        $content = Page::where('slug', 'checkout')->first();

        return view('checkout', compact('content'));
    }
}
