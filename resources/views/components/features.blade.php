<div class="block block-features block-features--layout--classic">
    <div class="container">
        <div class="block-features__list">
            @forelse ($benefits as $item)
            <div class="block-features__item">
                <div class="block-features__icon">
                    <svg width="48px" height="48px">
                        <use xlink:href="{{ $item->icon }}"></use>
                    </svg>
                </div>
                <div class="block-features__content">
                    <div class="block-features__title">{{ $item->title }} </div>
                    <div class="block-features__subtitle">{{ $item->short_description }}</div>
                </div>
            </div>
            <div class="block-features__divider"></div>
            @empty
                    
            @endforelse
        </div>
    </div>
</div>