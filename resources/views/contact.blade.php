@extends('layouts.app')

@section('content')

    <!-- site__body -->
    <div class="site__body">
        <div class="page-header">
            <div class="page-header__container container">
                {{-- bread --}}
                @include('components.bread')
                {{-- end bread --}}
                <div class="page-header__title">
                    <h1>Contact Us</h1>
                </div>
            </div>
        </div>
        <div class="block">
            <div class="container">
                <div class="card mb-0 contact-us">
                    <div class="contact-us__map">
                        <iframe src="https://maps.google.com/maps?q=Holbrook-Palmer%20Park&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                    </div>
                    <div class="card-body">
                        <div class="contact-us__container">
                            <div class="row">
                                <div class="col-12 col-lg-6 pb-4 pb-lg-0">
                                    <h4 class="contact-us__header card-title">Our Address</h4>
                                    <div class="contact-us__address">
                                        <p>715 Fake Ave, Apt. 34, New York, NY 10021 USA
                                            <br>Email: stroyka@example.com
                                            <br>Phone Number: +1 754 000-00-00</p>
                                        <p><strong>Opening Hours</strong>
                                            <br>Monday to Friday: 8am-8pm
                                            <br>Saturday: 8am-6pm
                                            <br>Sunday: 10am-4pm</p>
                                        <p><strong>Comment</strong>
                                            <br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur suscipit suscipit mi, non tempor nulla finibus eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <h4 class="contact-us__header card-title">Leave us a Message</h4>
                                    <form id="contact-form" action="{{ route('send-contacts') }}" method="POST">
                                        {!! csrf_field() !!}
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="form-name">Your Name</label>
                                                <input type="text" name="name" id="cont_name" class="form-control" placeholder="Your Name">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="form-email">Email</label>
                                                <input type="email" name="email" id="cont_email" class="form-control" placeholder="Email Address">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="form-subject">Subject</label>
                                                <input type="text" name="subject" id="cont_subject" class="form-control" placeholder="Subject">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="form-subject">Phone</label>
                                                <input type="text" name="phone" id="cont_phone" class="form-control" placeholder="Phone">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="form-message">Message</label>
                                            <textarea id="cont_msg" name="message" class="form-control" rows="4"></textarea>
                                        </div>
                                        <button id="contact_form_submit" type="submit" class="btn btn-primary">Send Message</button>
                                        <p class="input_error" id="contact_err" style="position: absolute; font-size: 12px; font-weight: 500; letter-spacing: 0.5px;"> </p>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- site__body / end -->
    
@endsection