<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Inani\LaravelNovaConfiguration\Helpers\Configuration;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $configurations = new Configuration();

        view()->share([
            'menu' => nova_get_menus(),
            'configurations' => $configurations,
        ]);
    }
}
