<?php

namespace App\Nova;

use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use Eminiarts\Tabs\Tabs;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Johnathan\NovaTrumbowyg\NovaTrumbowyg;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use Eminiarts\Tabs\TabsOnEdit;
use Benjacho\BelongsToManyField\BelongsToManyField;
use Hnassr\NovaKeyValue\KeyValue;
use Halimtuhu\ArrayImages\ArrayImages; 

class Product extends Resource
{
    use TabsOnEdit;
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Product';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            new Tabs('Tabs', [
                'Despre producte' => [
                    ID::make()->sortable(),
                    TextWithSlug::make('Title')
                        ->slug('slug'),
                    Text::make('SKU'),
                    Slug::make('Slug')->hideFromIndex(),
                    Textarea::make('Short description')->hideFromIndex()->rules('required'),
                    BelongsTo::make('Category Product', 'categories'),
                    BelongsToManyField::make('Color Product', 'colors', 'App\Nova\ColorProduct')
                        ->optionsLabel('value'),
                    BelongsToManyField::make('Type Product', 'types', 'App\Nova\TypeProduct')
                        ->hideFromIndex()
                        ->optionsLabel('type'),
                    BelongsTo::make('Brand Product', 'brands'),
                    Text::make('New price'),
                    Text::make('Old price'),
                    Text::make('Badge'),
                    Boolean::make('Status stock')->hideFromIndex(),
                    Boolean::make('Status'),
                    Boolean::make('Show on home')->hideFromIndex(),
                    Number::make('Order'),
                ],
                'Descrierea produs' => [
                    NovaTrumbowyg::make('Description')->hideFromIndex(),
                    KeyValue::make('Specification')->hideFromIndex(),
                    ArrayImages::make('Photos', 'photo')
                        ->disk('public'),
                    Image::make('Photo breadcrumbs')->hideFromIndex(),
                ],
                'SEO' => [
                    Textarea::make('Meta title'),
                    Textarea::make('Meta description')
                ]
            ])
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
