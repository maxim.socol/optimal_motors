<link rel="icon" type="image/png" href="{{asset('assets/images/favicon.png')}}">
<!-- fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i">
<!-- css -->
<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-4.2.1/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/owl-carousel-2.3.4/assets/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
<!-- font - fontawesome -->
<link rel="stylesheet" href="{{asset('assets/vendor/fontawesome-5.6.1/css/all.min.css')}}">
<!-- font - stroyka -->
<link rel="stylesheet" href="{{asset('assets/fonts/stroyka/stroyka.css')}}">