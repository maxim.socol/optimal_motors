
<div class="page-header__breadcrumb">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ $configurations->get('home-breadcrumb') }}</a>
                <svg class="breadcrumb-arrow" width="6px" height="9px">
                    <use xlink:href="{{asset('assets/images/sprite.svg#arrow-rounded-right-6x9')}}"></use>
                </svg>
            </li>
            <li class="breadcrumb-item"><a href="#">{{ $configurations->get('breadcrumb') }}</a>
                <svg class="breadcrumb-arrow" width="6px" height="9px">
                    <use xlink:href="{{asset('assets/images/sprite.svg#arrow-rounded-right-6x9')}}"></use>
                </svg>
            </li>
            <li class="breadcrumb-item active" aria-current="page">{{ $content->title }}</li>
        </ol>
    </nav>
</div>