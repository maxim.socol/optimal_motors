<?php

namespace App\Nova;

use Benjaminhirsch\NovaSlugField\Slug;
use Benjaminhirsch\NovaSlugField\TextWithSlug;
use Eminiarts\Tabs\Tabs;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Johnathan\NovaTrumbowyg\NovaTrumbowyg;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Textarea;
use Eminiarts\Tabs\TabsOnEdit;
use Laravel\Nova\Http\Requests\NovaRequest;

class Page extends Resource
{
    use TabsOnEdit;
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Page';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            new Tabs('Tabs', [
                'Despre pagina' => [
                    ID::make()->sortable(),
                    TextWithSlug::make('Title')
                        ->slug('slug'),
                    Slug::make('Slug')
                        ->showUrlPreview('https://optimal-motors.ro/'),
                    NovaTrumbowyg::make('Sub title')->hideFromIndex(),
                    NovaTrumbowyg::make('Description')->hideFromIndex(),
                    Image::make('Photo'),
                    Image::make('Photo breadcrumbs'),
                ],
                'SEO' => [
                    Textarea::make('Meta titlu', 'meta_title')->rules('required', 'max:255'),
                    Textarea::make('Meta descrierea', 'meta_description')->rules('required', 'max:255'),
                ]
            ])
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
