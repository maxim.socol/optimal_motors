<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;

class AccountController extends Controller
{
    public function account() {

        $content = Page::where('slug', 'about')->first();

        return view('account.account', compact('content'));
    }
}
