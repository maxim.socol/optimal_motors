@extends('layouts.app')

@section('content')
        <!-- site__body -->
    <div class="site__body">
        <div class="page-header">
            <div class="page-header__container container">
                {{-- bread --}}
                @include('components.bread')
                {{-- end bread --}}
                <div class="page-header__title">
                    <h1>Electrogen</h1>
                </div>
            </div>
        </div>

        <div class="container">
                <div class="shop-layout shop-layout--sidebar--start">
                    {{-- sidebar --}}
                    @include('products.sidebar')
                    {{-- end sidebar --}}
                    
                    <div class="shop-layout__content">
                        <div class="block">
                            <div class="products-view">
                                {{-- options --}}
                                @include('products.options')
                                {{-- end options --}}
                                <div class="products-view__list products-list" data-layout="grid-3-sidebar"
                                    data-with-features="false">
                                    <div class="products-list__body">
                                        @forelse ($products as $item)   
                                            <div class="products-list__item">
                                                <div class="product-card">
                                                    <button class="product-card__quickview" type="button">
                                                        <svg width="16px" height="16px">
                                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                                        </svg> <span class="fake-svg-icon"></span></button>
                                                    <div class="product-card__badges-list">
                                                        <div class="product-card__badge product-card__badge--{{ $item->badge }}">{{ $configurations->get($item->badge) }}</div>
                                                    </div>
                                                    <div class="product-card__image">
                                                        <a href="{{ route('products', $item->slug) }}">
                                                            <img src="/storage/{{ $item->photo }}" alt="{{ $item->title }}">
                                                        </a>
                                                    </div>
                                                    <div class="product-card__info">
                                                        <div class="product-card__name">
                                                            <a href="{{ route('products', $item->slug) }}">{{ $item->title }}</a>
                                                        </div>
                                                        <ul class="product-card__features-list">
                                                            <li>Speed: 750 RPM</li>
                                                            <li>Power Source: Cordless-Electric</li>
                                                            <li>Battery Cell Type: Lithium</li>
                                                            <li>Voltage: 20 Volts</li>
                                                            <li>Battery Capacity: 2 Ah</li>
                                                        </ul>
                                                    </div>
                                                    <div class="product-card__actions">
                                                        @if($item->status_stock == 1)
                                                            <div class="product-card__availability">Disponibilitate: 
                                                                <span class="text-success">In Stock</span>
                                                            </div>
                                                        @else
                                                            <div class="product-card__availability">Disponibilitate: 
                                                                <span class="text-success">Out of stock</span>
                                                            </div>
                                                        @endif
                                                        <div class="product-card__prices">
                                                            @if($item->old_price == '')
                                                                <span class="product-card__prices">{{ $item->new_price }} LEI</span> 
                                                            @else
                                                                <span class="product-card__old-price">{{ $item->old_price }} LEI</span>
                                                            @endif
                                                            {{-- <span class="product-card__new-price">999.99 LEI</span> 
                                                            <span class="product-card__old-price">999.99 LEI</span> --}}
                                                        </div>
                                                        <div class="product-card__buttons">
                                                            <button class="btn btn-primary product-card__addtocart"
                                                                type="button">Adauga in cos</button>
                                                            <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                                type="button">Adauga in cos</button>
                                                            <button
                                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                                type="button">
                                                                <svg width="16px" height="16px">
                                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                                </svg> 
                                                                <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @empty

                                        @endforelse
                                    </div>
                                </div>
                                <div class="products-view__pagination">
                                    {{ $products->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection