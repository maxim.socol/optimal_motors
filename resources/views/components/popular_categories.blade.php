<div class="block block--highlighted block-categories block-categories--layout--classic">
    <div class="container">
        <div class="block-header">
            <h3 class="block-header__title">{{ $configurations->get('categorii-populare') }}</h3>
            <div class="block-header__divider"></div>
        </div>
        <div class="block-categories__list">
            @forelse ($popular_categories as $item)
                <div class="block-categories__item category-card category-card--layout--classic">
                    <div class="category-card__body">
                        <div class="category-card__image">
                            <a href="#"><img src="/storage/{{ $item->photo }}" alt=""></a>
                        </div>
                        <div class="category-card__content">
                            <div class="category-card__name"><a href="{{ route('category', $item->slug) }}">{{ $item->name }}</a></div>
                            <div class="category-card__products">572 Products</div>
                            <div class="category-card__all"><a href="{{ route('category', $item->slug) }}">Show All</a></div>
                        </div>
                    </div>
                </div>
            @empty
                
            @endforelse
        </div>
    </div>
</div>