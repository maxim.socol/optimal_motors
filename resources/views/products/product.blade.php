@extends('layouts.app')

@section('content')
    <!-- site__body -->
    <div class="site__body">
        <div class="page-header">
            <div class="page-header__container container">
                {{-- bread --}}
                @include('components.bread')
                {{-- end bread --}}
            </div>
        </div>
        <div class="block">
            <div class="container">
                <div class="product product--layout--standard" data-layout="standard">
                    <div class="product__content">
                        <!-- .product__gallery -->
                        <div class="product__gallery">
                            <div class="product-gallery">
                                <div class="product-gallery__featured">
                                    <div class="owl-carousel" id="product-image">
                                        @php $images = json_decode($content->photo); @endphp
                                        @if(isset($images))
                                            @foreach ($images as $image)
                                                <a href="{{ $image->url }}" target="_blank">
                                                    <img src="{{ $image->url }}" alt="{{ $content->title }}">
                                                </a>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="product-gallery__carousel">
                                    <div class="owl-carousel" id="product-carousel">
                                        @php $images = json_decode($content->photo); @endphp
                                        @if(isset($images))
                                            @foreach ($images as $image)
                                                <a href="{{ $image->url }}" class="product-gallery__carousel-item">
                                                    <img class="product-gallery__carousel-image"
                                                    src="{{ $image->url }}" alt="{{ $content->title }}"> 
                                                </a>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- .product__gallery / end -->
                        <!-- .product__info -->
                        <div class="product__info">
                            <div class="product__wishlist-compare">
                                <button type="button" class="btn btn-sm btn-light btn-svg-icon"
                                    data-toggle="tooltip" data-placement="right" title="Wishlist">
                                    <svg width="16px" height="16px">
                                        <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                    </svg>
                                </button>
                                <button type="button" class="btn btn-sm btn-light btn-svg-icon"
                                    data-toggle="tooltip" data-placement="right" title="Compare">
                                    <svg width="16px" height="16px">
                                        <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                    </svg>
                                </button>
                            </div>
                            <h1 class="product__name">{{ $content->title }}</h1>
                            <div class="product__description">{{ $content->short_description }}</div>
                            <ul class="product__features">
                                <li>Speed: 750 RPM</li>
                                <li>Power Source: Cordless-Electric</li>
                                <li>Battery Cell Type: Lithium</li>
                                <li>Voltage: 20 Volts</li>
                                <li>Battery Capacity: 2 Ah</li>
                            </ul>
                            <ul class="product__meta">
                                @if($content->status_stock == 1)
                                    <li class="product__meta-availability">Availability: 
                                        <span class="text-success">In Stock</span>
                                    </li>
                                @else
                                    <li class="product__meta-availability">Availability: 
                                        <span class="text-success">Out of stock</span>
                                    </li>
                                @endif
                                <li>Brand: <a href="#">{{ $content->brands->name }}</a></li>
                                <li>SKU: {{ $content->sku }}</li>
                            </ul>
                        </div>
                        <!-- .product__info / end -->
                        <!-- .product__sidebar -->
                        <div class="product__sidebar">
                            <div class="product__availability">Accesibilitatea: 
                                <span class="text-success">In Stock</span>
                            </div>
                            <div class="product__prices">{{ $content->old_price }}Lei</div>
                            <!-- .product__options -->
                            <form class="product__options">
                                <div class="form-group product__option">
                                    <label class="product__option-label">Culoarea</label>
                                    <div class="input-radio-color">
                                        <div class="input-radio-color__list">
                                            @forelse ($content->colors as $color)
                                                <label class="input-radio-color__item input-radio-color__item--white"
                                                    style="color: {{ $color->color }};" data-toggle="tooltip" title="{{ $color->value }}">
                                                    <input type="radio" name="color"> <span></span>
                                                </label>
                                            @empty
											 
										    @endforelse
                                            <label class="input-radio-color__item input-radio-color__item--disabled"
                                                style="color: #4080ff;" data-toggle="tooltip" title="Blue">
                                                <input type="radio" name="color" disabled="disabled">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group product__option">
                                    <label class="product__option-label">Tip</label>
                                    <div class="input-radio-label">
                                        <div class="input-radio-label__list">
                                            @forelse ($content->types as $type)
                                                <label>
                                                    <input type="radio" name="material"> <span>{{ $type->type }}</span>
                                                </label>
                                            @empty

                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group product__option">
                                    <label class="product__option-label" for="product-quantity">CANTITATE</label>
                                    <div class="product__actions">
                                        <div class="product__actions-item">
                                            <div class="input-number product__quantity">
                                                <input id="product-quantity"
                                                    class="input-number__input form-control form-control-lg"
                                                    type="number" min="1" value="1">
                                                <div class="input-number__add"></div>
                                                <div class="input-number__sub"></div>
                                            </div>
                                        </div>
                                        <div class="product__actions-item product__actions-item--addtocart">
                                            <button class="btn btn-primary btn-lg">Adauga in cos</button>
                                        </div>
                                        <div class="product__actions-item product__actions-item--wishlist">
                                            <button type="button" class="btn btn-secondary btn-svg-icon btn-lg"
                                                data-toggle="tooltip" title="Wishlist">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- .product__options / end -->
                        </div>
                        <!-- .product__end -->
                    </div>
                </div>
                <div class="product-tabs">
                    <div class="product-tabs__list">
                        <a href="#tab-description" class="product-tabs__item product-tabs__item--active">Descrierea</a> 
                        <a href="#tab-specification" class="product-tabs__item">Specificatie</a>
                    </div>
                    <div class="product-tabs__content">
                        <div class="product-tabs__pane product-tabs__pane--active" id="tab-description">
                            <div class="typography">
                                {!! $content->description !!}
                            </div>
                        </div>
                        <div class="product-tabs__pane" id="tab-specification">
                            <div class="spec">
                                <h3 class="spec__header">Specificatie</h3>
                                <div class="spec__section">
                                    <h4 class="spec__section-title">Generale</h4>
                                    @php $spec = json_decode($content->specification) @endphp
                                    @forelse ($spec as $key => $value)
                                        <div class="spec__row">
                                            <div class="spec__name">{{ $value->key }}</div>
                                            <div class="spec__value">{{ $value->value }}</div>
                                        </div>
                                    @empty
                                        
                                    @endforelse
                                </div>
                        </div>
                        <div class="product-tabs__pane" id="tab-reviews">
                            <div class="reviews-view">
                                <div class="reviews-view__list">
                                    <h3 class="reviews-view__header">Customer Reviews</h3>
                                    <div class="reviews-list">
                                        <ol class="reviews-list__content">
                                            <li class="reviews-list__item">
                                                <div class="review">
                                                    <div class="review__avatar"><img
                                                            src="{{asset('assets/images/avatars/avatar-1.jpg')}}" alt=""></div>
                                                    <div class="review__content">
                                                        <div class="review__author">Samantha Smith</div>
                                                        <div class="review__rating">
                                                            <div class="rating">
                                                                <div class="rating__body">
                                                                    <svg class="rating__star rating__star--active"
                                                                        width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal')}}">
                                                                            </use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal-stroke')}}">
                                                                            </use>
                                                                        </g>
                                                                    </svg>
                                                                    <div
                                                                        class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star rating__star--active"
                                                                        width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal')}}">
                                                                            </use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal-stroke')}}">
                                                                            </use>
                                                                        </g>
                                                                    </svg>
                                                                    <div
                                                                        class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star rating__star--active"
                                                                        width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal')}}">
                                                                            </use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal-stroke')}}">
                                                                            </use>
                                                                        </g>
                                                                    </svg>
                                                                    <div
                                                                        class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star rating__star--active"
                                                                        width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal')}}">
                                                                            </use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal-stroke')}}">
                                                                            </use>
                                                                        </g>
                                                                    </svg>
                                                                    <div
                                                                        class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star" width="13px"
                                                                        height="12px">
                                                                        <g class="rating__fill">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal')}}">
                                                                            </use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal-stroke')}}">
                                                                            </use>
                                                                        </g>
                                                                    </svg>
                                                                    <div
                                                                        class="rating__star rating__star--only-edge">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="review__text">Phasellus id mattis nulla. Mauris
                                                            velit nisi, imperdiet vitae sodales in, maximus ut
                                                            lectus. Vivamus commodo scelerisque lacus, at porttitor
                                                            dui iaculis id. Curabitur imperdiet ultrices fermentum.
                                                        </div>
                                                        <div class="review__date">27 May, 2018</div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="reviews-list__item">
                                                <div class="review">
                                                    <div class="review__avatar"><img
                                                            src="{{asset('assets/images/avatars/avatar-2.jpg')}}" alt=""></div>
                                                    <div class="review__content">
                                                        <div class="review__author">Adam Taylor</div>
                                                        <div class="review__rating">
                                                            <div class="rating">
                                                                <div class="rating__body">
                                                                    <svg class="rating__star rating__star--active"
                                                                        width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal')}}">
                                                                            </use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal-stroke')}}">
                                                                            </use>
                                                                        </g>
                                                                    </svg>
                                                                    <div
                                                                        class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star rating__star--active"
                                                                        width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal')}}">
                                                                            </use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal-stroke')}}">
                                                                            </use>
                                                                        </g>
                                                                    </svg>
                                                                    <div
                                                                        class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star rating__star--active"
                                                                        width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal')}}">
                                                                            </use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal-stroke')}}">
                                                                            </use>
                                                                        </g>
                                                                    </svg>
                                                                    <div
                                                                        class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star" width="13px"
                                                                        height="12px">
                                                                        <g class="rating__fill">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal')}}">
                                                                            </use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal-stroke')}}">
                                                                            </use>
                                                                        </g>
                                                                    </svg>
                                                                    <div
                                                                        class="rating__star rating__star--only-edge">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star" width="13px"
                                                                        height="12px">
                                                                        <g class="rating__fill">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal')}}">
                                                                            </use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal-stroke')}}">
                                                                            </use>
                                                                        </g>
                                                                    </svg>
                                                                    <div
                                                                        class="rating__star rating__star--only-edge">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="review__text">Aenean non lorem nisl. Duis tempor
                                                            sollicitudin orci, eget tincidunt ex semper sit amet.
                                                            Nullam neque justo, sodales congue feugiat ac, facilisis
                                                            a augue. Donec tempor sapien et fringilla facilisis. Nam
                                                            maximus consectetur diam. Nulla ut ex mollis, volutpat
                                                            tellus vitae, accumsan ligula.</div>
                                                        <div class="review__date">12 April, 2018</div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="reviews-list__item">
                                                <div class="review">
                                                    <div class="review__avatar"><img
                                                            src="{{asset('assets/images/avatars/avatar-3.jpg')}}" alt=""></div>
                                                    <div class="review__content">
                                                        <div class="review__author">Helena Garcia</div>
                                                        <div class="review__rating">
                                                            <div class="rating">
                                                                <div class="rating__body">
                                                                    <svg class="rating__star rating__star--active"
                                                                        width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal')}}">
                                                                            </use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal-stroke')}}">
                                                                            </use>
                                                                        </g>
                                                                    </svg>
                                                                    <div
                                                                        class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star rating__star--active"
                                                                        width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal')}}">
                                                                            </use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal-stroke')}}">
                                                                            </use>
                                                                        </g>
                                                                    </svg>
                                                                    <div
                                                                        class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star rating__star--active"
                                                                        width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal')}}">
                                                                            </use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal-stroke')}}">
                                                                            </use>
                                                                        </g>
                                                                    </svg>
                                                                    <div
                                                                        class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star rating__star--active"
                                                                        width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal')}}">
                                                                            </use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal-stroke')}}">
                                                                            </use>
                                                                        </g>
                                                                    </svg>
                                                                    <div
                                                                        class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                    <svg class="rating__star rating__star--active"
                                                                        width="13px" height="12px">
                                                                        <g class="rating__fill">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal')}}">
                                                                            </use>
                                                                        </g>
                                                                        <g class="rating__stroke">
                                                                            <use
                                                                                xlink:href="{{asset('assets/images/sprite.svg#star-normal-stroke')}}">
                                                                            </use>
                                                                        </g>
                                                                    </svg>
                                                                    <div
                                                                        class="rating__star rating__star--only-edge rating__star--active">
                                                                        <div class="rating__fill">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                        <div class="rating__stroke">
                                                                            <div class="fake-svg-icon"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="review__text">Duis ac lectus scelerisque quam
                                                            blandit egestas. Pellentesque hendrerit eros laoreet
                                                            suscipit ultrices.</div>
                                                        <div class="review__date">2 January, 2018</div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ol>
                                        <div class="reviews-list__pagination">
                                            <ul class="pagination justify-content-center">
                                                <li class="page-item disabled">
                                                    <a class="page-link page-link--with-arrow" href="#"
                                                        aria-label="Previous">
                                                        <svg class="page-link__arrow page-link__arrow--left"
                                                            aria-hidden="true" width="8px" height="13px">
                                                            <use
                                                                xlink:href="{{asset('assets/images/sprite.svg#arrow-rounded-left-8x13')}}">
                                                            </use>
                                                        </svg>
                                                    </a>
                                                </li>
                                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                <li class="page-item active"><a class="page-link" href="#">2 <span
                                                            class="sr-only">(current)</span></a></li>
                                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                                <li class="page-item">
                                                    <a class="page-link page-link--with-arrow" href="#"
                                                        aria-label="Next">
                                                        <svg class="page-link__arrow page-link__arrow--right"
                                                            aria-hidden="true" width="8px" height="13px">
                                                            <use
                                                                xlink:href="{{asset('assets/images/sprite.svg#arrow-rounded-right-8x13')}}">
                                                            </use>
                                                        </svg>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <form class="reviews-view__form">
                                    <h3 class="reviews-view__header">Write A Review</h3>
                                    <div class="row">
                                        <div class="col-12 col-lg-9 col-xl-8">
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="review-stars">Review Stars</label>
                                                    <select id="review-stars" class="form-control">
                                                        <option>5 Stars Rating</option>
                                                        <option>4 Stars Rating</option>
                                                        <option>3 Stars Rating</option>
                                                        <option>2 Stars Rating</option>
                                                        <option>1 Stars Rating</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="review-author">Your Name</label>
                                                    <input type="text" class="form-control" id="review-author"
                                                        placeholder="Your Name">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="review-email">Email Address</label>
                                                    <input type="text" class="form-control" id="review-email"
                                                        placeholder="Email Address">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="review-text">Your Review</label>
                                                <textarea class="form-control" id="review-text" rows="6"></textarea>
                                            </div>
                                            <div class="form-group mb-0">
                                                <button type="submit" class="btn btn-primary btn-lg">Post Your
                                                    Review</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .block-products-carousel -->
        <div class="block block-products-carousel" data-layout="grid-5" style="margin-top: 50px;">
            <div class="container">
                <div class="block-header">
                    <h3 class="block-header__title">Produse Similare</h3>
                    <div class="block-header__divider"></div>
                    <div class="block-header__arrows-list">
                        <button class="block-header__arrow block-header__arrow--left" type="button">
                            <svg width="7px" height="11px">
                                <use xlink:href="{{asset('assets/images/sprite.svg#arrow-rounded-left-7x11')}}"></use>
                            </svg>
                        </button>
                        <button class="block-header__arrow block-header__arrow--right" type="button">
                            <svg width="7px" height="11px">
                                <use xlink:href="{{asset('assets/images/sprite.svg#arrow-rounded-right-7x11')}}"></use>
                            </svg>
                        </button>
                    </div>
                </div>
                <div class="block-products-carousel__slider">
                    <div class="block-products-carousel__preloader"></div>
                    <div class="owl-carousel">
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__badges-list">
                                        <div class="product-card__badge product-card__badge--new">NOU</div>
                                    </div>
                                    <div class="product-card__image">
                                        <a href="/product"><img src="{{asset('assets/images/products/generator-honda-eu-22-it.jpg')}}" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="product.html">Grup electrogen EU 22 iT</a></div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">999.99 LEI</div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__badges-list">
                                        <div class="product-card__badge product-card__badge--hot">Hot</div>
                                    </div>
                                    <div class="product-card__image">
                                        <a href="product.html"><img src="{{asset('assets/images/products/honda-eu30ik1.jpg')}}" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="product.html">Grup electrogen EU30 iK1</a></div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">999.99 LEI</div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__image">
                                        <a href="#"><img src="https://www.tehnocenter.ro/_res/_thumbs/1024x768/promo/honda/generatoare/eu-30-is-generator-digital-honda.1486203667.jpg" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="#">Grup electrogen EU 30 iS</a></div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">999.99 LEI</div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__badges-list">
                                        <div class="product-card__badge product-card__badge--sale">Sale</div>
                                    </div>
                                    <div class="product-card__image">
                                        <a href="product.html"><img src="http://pravaliamagica.ro/prod_pics/5GeeRqOZhTJnA2e.jpg" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="product.html">Grup electrogen HW 220</a></div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices"><span
                                                class="product-card__new-price">999.99 LEI</span> <span
                                                class="product-card__old-price">999.99 LEI</span></div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__image">
                                        <a href="product.html"><img src="{{asset('assets/images/products/generator-honda-eu-22-it.jpg')}}" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="product.html">Brandix Router Power
                                                Tool 2017ERXPK</a></div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">$1,700.00</div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__image">
                                        <a href="product.html"><img src="{{asset('assets/images/products/product-6.jpg')}}" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="product.html">Brandix Drilling
                                                Machine DM2019KW4 4kW</a></div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">$3,199.00</div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__image">
                                        <a href="product.html"><img src="{{asset('assets/images/products/product-7.jpg')}}" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="product.html">Brandix Pliers</a>
                                        </div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">$24.00</div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__image">
                                        <a href="product.html"><img src="{{asset('assets/images/products/product-8.jpg')}}" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="product.html">Water Hose 40cm</a>
                                        </div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">$15.00</div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__image">
                                        <a href="product.html"><img src="{{asset('assets/images/products/product-9.jpg')}}" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="product.html">Spanner Wrench</a>
                                        </div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">$19.00</div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__image">
                                        <a href="product.html"><img src="{{asset('assets/images/products/product-10.jpg')}}" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="product.html">Water Tap</a></div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">$15.00</div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__image">
                                        <a href="product.html"><img src="{{asset('assets/images/products/product-11.jpg')}}" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="product.html">Hand Tool Kit</a>
                                        </div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">$149.00</div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__image">
                                        <a href="product.html"><img src="{{asset('assets/images/products/product-12.jpg')}}" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="product.html">Ash's Chainsaw
                                                3.5kW</a></div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">$666.99</div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__image">
                                        <a href="product.html"><img src="{{asset('assets/images/products/product-13.jpg')}}" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="product.html">Brandix Angle Grinder
                                                KZX3890PQW</a></div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">$649.00</div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__image">
                                        <a href="product.html"><img src="{{asset('assets/images/products/product-14.jpg')}}" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="product.html">Brandix Air
                                                Compressor DELTAKX500</a></div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">$1,800.00</div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__image">
                                        <a href="product.html"><img src="{{asset('assets/images/products/product-15.jpg')}}" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="product.html">Brandix Electric
                                                Jigsaw JIG7000BQ</a></div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">$290.00</div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block-products-carousel__column">
                            <div class="block-products-carousel__cell">
                                <div class="product-card">
                                    <button class="product-card__quickview" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{asset('assets/images/sprite.svg#quickview-16')}}"></use>
                                        </svg> <span class="fake-svg-icon"></span></button>
                                    <div class="product-card__image">
                                        <a href="product.html"><img src="{{asset('assets/images/products/generator-honda-eu-22-it.jpg')}}" alt=""></a>
                                    </div>
                                    <div class="product-card__info">
                                        <div class="product-card__name"><a href="product.html">Brandix Screwdriver
                                                SCREW1500ACC</a></div>
                                        <ul class="product-card__features-list">
                                            <li>Speed: 750 RPM</li>
                                            <li>Power Source: Cordless-Electric</li>
                                            <li>Battery Cell Type: Lithium</li>
                                            <li>Voltage: 20 Volts</li>
                                            <li>Battery Capacity: 2 Ah</li>
                                        </ul>
                                    </div>
                                    <div class="product-card__actions">
                                        <div class="product-card__availability">Availability: <span
                                                class="text-success">In Stock</span></div>
                                        <div class="product-card__prices">$1,499.00</div>
                                        <div class="product-card__buttons">
                                            <button class="btn btn-primary product-card__addtocart"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                type="button">Adauga in cos</button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#wishlist-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--wishlist-16"></span></button>
                                            <button
                                                class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare"
                                                type="button">
                                                <svg width="16px" height="16px">
                                                    <use xlink:href="{{asset('assets/images/sprite.svg#compare-16')}}"></use>
                                                </svg> <span
                                                    class="fake-svg-icon fake-svg-icon--compare-16"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .block-products-carousel / end -->
    </div>
    <!-- site__body / end -->
@endsection