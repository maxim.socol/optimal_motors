<div class="products-view__options">
    <div class="view-options">
        <div class="view-options__layout">
            <div class="layout-switcher">
                <div class="layout-switcher__list">
                    <button data-layout="grid-3-sidebar" data-with-features="false"
                        title="Grid" type="button"
                        class="layout-switcher__button layout-switcher__button--active">
                        <svg width="16px" height="16px">
                            <use xlink:href="{{asset('assets/images/sprite.svg#layout-grid-16x16')}}"></use>
                        </svg>
                    </button>
                    <button data-layout="grid-3-sidebar" data-with-features="true"
                        title="Grid With Features" type="button"
                        class="layout-switcher__button">
                        <svg width="16px" height="16px">
                            <use
                                xlink:href="{{asset('assets/images/sprite.svg#layout-grid-with-details-16x16')}}">
                            </use>
                        </svg>
                    </button>
                    <button data-layout="list" data-with-features="false" title="List"
                        type="button" class="layout-switcher__button">
                        <svg width="16px" height="16px">
                            <use xlink:href="{{asset('assets/images/sprite.svg#layout-list-16x16')}}"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
        <div class="view-options__legend">Afișare 6 din 98 de produse</div>
        <div class="view-options__divider"></div>
        <div class="view-options__control">
            <label for="">Sortarea dupa</label>
            <div>
                <select class="form-control form-control-sm" name="" id="">
                    <option value="">Default</option>
                    <option value="">Nume (A-Z)</option>
                </select>
            </div>
        </div>
        <div class="view-options__control">
            <label for="">Arată</label>
            <div>
                <select class="form-control form-control-sm" name="" id="">
                    <option value="">12</option>
                    <option value="">24</option>
                </select>
            </div>
        </div>
    </div>
</div>