<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class ColorProduct extends Model
{
    protected $table = 'color_products';
    protected $guarded = [''];

    public function products() {

        return $this->belongsToMany(Product::class);
    }
}
