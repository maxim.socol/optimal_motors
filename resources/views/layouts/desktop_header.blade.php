<header class="site__header d-lg-block d-none">
    <div class="site-header">
        <!-- .topbar -->
        <div class="site-header__topbar topbar">
            <div class="topbar__container container">
                <div class="topbar__row">
                    <div class="topbar__item topbar__item--link"><a class="topbar-link" href="{{ url('about') }}">Despre noi</a></div>
                    <div class="topbar__item topbar__item--link"><a class="topbar-link" href="{{ url('contact') }}">Contact</a></div>
                    <div class="topbar__item topbar__item--link"><a class="topbar-link" href="#">Locatia noastră</a></div>
                    <div class="topbar__item topbar__item--link"><a class="topbar-link" href="#">Vezi comanda</a></div>
                    <div class="topbar__item topbar__item--link"><a class="topbar-link" href="{{ url('faq') }}">FAQ</a></div>
                    <div class="topbar__spring"></div>
                    <div class="topbar__item">
                        <div class="topbar-dropdown">
                            <button class="topbar-dropdown__btn" type="button">Contul meu
                                <svg width="7px" height="5px">
                                    <use xlink:href="{{asset('assets/images/sprite.svg#arrow-rounded-down-7x5')}}"></use>
                                </svg>
                            </button>
                            <div class="topbar-dropdown__body">
                                <!-- .menu -->
                                <ul class="menu menu--layout--topbar">
                                    <li><a href="account.html">Login</a></li>
                                    <li><a href="account.html">Register</a></li>
                                    <li><a href="#">Orders</a></li>
                                    <li><a href="#">Addresses</a></li>
                                </ul>
                                <!-- .menu / end -->
                            </div>
                        </div>
                    </div>
                    {{-- <div class="topbar__item">
                        <div class="topbar-dropdown">
                            <button class="topbar-dropdown__btn" type="button">Currency: <span class="topbar__item-value">USD</span>
                                <svg width="7px" height="5px">
                                    <use xlink:href="{{asset('assets/images/sprite.svg#arrow-rounded-down-7x5')}}"></use>
                                </svg>
                            </button>
                            <div class="topbar-dropdown__body">
                                <!-- .menu -->
                                <ul class="menu menu--layout--topbar">
                                    <li><a href="#">€ Euro</a></li>
                                    <li><a href="#">£ Pound Sterling</a></li>
                                    <li><a href="#">$ US Dollar</a></li>
                                    <li><a href="#">₽ Russian Ruble</a></li>
                                </ul>
                                <!-- .menu / end -->
                            </div>
                        </div>
                    </div>
                    <div class="topbar__item">
                        <div class="topbar-dropdown">
                            <button class="topbar-dropdown__btn" type="button">Language: <span class="topbar__item-value">EN</span>
                                <svg width="7px" height="5px">
                                    <use xlink:href="{{asset('assets/images/sprite.svg#arrow-rounded-down-7x5')}}"></use>
                                </svg>
                            </button>
                            <div class="topbar-dropdown__body">
                                <!-- .menu -->
                                <ul class="menu menu--layout--topbar menu--with-icons">
                                    <li>
                                        <a href="#">
                                            <div class="menu__icon"><img srcset="{{asset('assets/images/languages/language-1.png')}}, {{asset('assets/images/languages/language-1@2x.png 2x')}}" src="{{asset('assets/images/languages/language-1.png')}}" alt=""></div>English</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="menu__icon"><img srcset="{{asset('assets/images/languages/language-2.png')}}, {{asset('assets/images/languages/language-2@2x.png 2x')}}" src="{{asset('assets/images/languages/language-2.png')}}" alt=""></div>French</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="menu__icon"><img srcset="{{asset('assets/images/languages/language-3.png')}}, {{asset('assets/images/languages/language-3@2x.png 2x')}}" src="{{asset('assets/images/languages/language-3.png')}}" alt=""></div>German</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="menu__icon"><img srcset="{{asset('assets/images/languages/language-4.png')}}, {{asset('assets/images/languages/language-4@2x.png 2x')}}" src="{{asset('assets/images/languages/language-4.png')}}" alt=""></div>Russian</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="menu__icon"><img srcset="{{asset('assets/images/languages/language-5.png')}}, {{asset('assets/images/languages/language-5@2x.png 2x')}}" src="{{asset('assets/images/languages/language-5.png')}}" alt=""></div>Italian</a>
                                    </li>
                                </ul>
                                <!-- .menu / end -->
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
        <!-- .topbar / end -->
        <div class="site-header__middle container">
            <div class="site-header__logo">
                <a href="{{ url('/') }}">
                    <img src="{{asset('assets/images/logo_new.png')}}" alt="">
                </a>
            </div>
            <div class="site-header__search">
                <div class="search">
                    <form class="search__form" action="#">
                        <input class="search__input" name="search" placeholder="{{ $configurations->get('alege-produse') }}" aria-label="Site search" type="text" autocomplete="off">
                        <button class="search__button" type="submit">
                            <svg width="20px" height="20px">
                                <use xlink:href="{{asset('assets/images/sprite.svg#search-20')}}"></use>
                            </svg>
                        </button>
                        <div class="search__border"></div>
                    </form>
                </div>
            </div>
            <div class="site-header__phone">
                <div class="site-header__phone-title">{{ $configurations->get('relatii-cu-client') }}</div>
                <div class="site-header__phone-number">{{ $configurations->get('phone') }}</div>
            </div>
        </div>
        <div class="site-header__nav-panel">
            <div class="nav-panel">
                <div class="nav-panel__container container">
                    <div class="nav-panel__row">
                        <div class="nav-panel__departments">
                            <!-- .departments -->
                            <div class="departments @if(request()->segment(1) == '') departments--opened departments--fixed @endif" data-departments-fixed-by=".block-slideshow">
                                <div class="departments__body">
                                    <div class="departments__links-wrapper">
                                        <ul class="departments__links">
                                            @foreach ($categories as $item)
                                                <li class="departments__item">
                                                    <a href="{{ route('category', $item->slug) }}">{{ $item->name }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <button class="departments__button">
                                    <svg class="departments__button-icon" width="18px" height="14px">
                                        <use xlink:href="{{ asset('assets/images/sprite.svg#menu-18x14') }}"></use>
                                    </svg> Shop By Category
                                    <svg class="departments__button-arrow" width="9px" height="6px">
                                        <use xlink:href="{{ asset('assets/images/sprite.svg#arrow-rounded-down-9x6') }}"></use>
                                    </svg>
                                </button>
                            </div>
                            <!-- .departments / end -->
                        </div>
                        <!-- .nav-links -->
                        <div class="nav-panel__nav-links nav-links">
                            <ul class="nav-links__list">
                                <li class="nav-links__item nav-links__item--with-submenu"><a href="{{ url('/') }}"><span>Acasa</span></a>
                                    {{-- <svg class="nav-links__arrow" width="9px" height="6px"><use xlink:href="{{ asset('assets/images/sprite.svg#arrow-rounded-down-9x6') }}"></use></svg></span> --}}
                                    {{-- <div class="nav-links__menu">
                                        <!-- .menu -->
                                        <ul class="menu menu--layout--classic">
                                            <li><a href="index.html">Home 1</a></li>
                                            <li><a href="index-2.html">Home 2</a></li>
                                        </ul>
                                        <!-- .menu / end -->
                                    </div> --}}
                                </li>
                                <li class="nav-links__item nav-links__item--with-submenu"><a href="{{ url('about') }}"><span>Despre noi</span></a>
                                    {{-- <svg class="nav-links__arrow" width="9px" height="6px"><use xlink:href="{{ asset('assets/images/sprite.svg#arrow-rounded-down-9x6') }}"></use></svg> --}}
                                    {{-- <div class="nav-links__megamenu nav-links__megamenu--size--nl">
                                        <!-- .megamenu -->
                                        <div class="megamenu">
                                            <div class="row">
                                                <div class="col-6">
                                                    <ul class="megamenu__links megamenu__links--level--0">
                                                        <li class="megamenu__item megamenu__item--with-submenu"><a href="#">Power Tools</a>
                                                            <ul class="megamenu__links megamenu__links--level--1">
                                                                <li class="megamenu__item"><a href="#">Engravers</a></li>
                                                                <li class="megamenu__item"><a href="#">Wrenches</a></li>
                                                                <li class="megamenu__item"><a href="#">Wall Chaser</a></li>
                                                                <li class="megamenu__item"><a href="#">Pneumatic Tools</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="megamenu__item megamenu__item--with-submenu"><a href="#">Machine Tools</a>
                                                            <ul class="megamenu__links megamenu__links--level--1">
                                                                <li class="megamenu__item"><a href="#">Thread Cutting</a></li>
                                                                <li class="megamenu__item"><a href="#">Chip Blowers</a></li>
                                                                <li class="megamenu__item"><a href="#">Sharpening Machines</a></li>
                                                                <li class="megamenu__item"><a href="#">Pipe Cutters</a></li>
                                                                <li class="megamenu__item"><a href="#">Slotting machines</a></li>
                                                                <li class="megamenu__item"><a href="#">Lathes</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-6">
                                                    <ul class="megamenu__links megamenu__links--level--0">
                                                        <li class="megamenu__item megamenu__item--with-submenu"><a href="#">Hand Tools</a>
                                                            <ul class="megamenu__links megamenu__links--level--1">
                                                                <li class="megamenu__item"><a href="#">Screwdrivers</a></li>
                                                                <li class="megamenu__item"><a href="#">Handsaws</a></li>
                                                                <li class="megamenu__item"><a href="#">Knives</a></li>
                                                                <li class="megamenu__item"><a href="#">Axes</a></li>
                                                                <li class="megamenu__item"><a href="#">Multitools</a></li>
                                                                <li class="megamenu__item"><a href="#">Paint Tools</a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="megamenu__item megamenu__item--with-submenu"><a href="#">Garden Equipment</a>
                                                            <ul class="megamenu__links megamenu__links--level--1">
                                                                <li class="megamenu__item"><a href="#">Motor Pumps</a></li>
                                                                <li class="megamenu__item"><a href="#">Chainsaws</a></li>
                                                                <li class="megamenu__item"><a href="#">Electric Saws</a></li>
                                                                <li class="megamenu__item"><a href="#">Brush Cutters</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- .megamenu / end -->
                                    </div> --}}
                                </li>
                                <li class="nav-links__item nav-links__item--with-submenu">
                                    <a href="{{ url('products') }}">
                                        <span>Magazin</span>
                                    </a>
                                </li>
                                <li class="nav-links__item nav-links__item--with-submenu">
                                    <a href="{{ url('faq') }}">
                                        <span>FAQ</span>
                                    </a>
                                </li>
                                <li class="nav-links__item nav-links__item--with-submenu">
                                    <a href="{{ url('contact') }}">
                                        <span>Contacteaza-ne</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- .nav-links / end -->
                        <div class="nav-panel__indicators">
                            <div class="indicator"><a href="wishlist.html" class="indicator__button"><span class="indicator__area"><svg width="20px" height="20px"><use xlink:href="{{ asset('assets/images/sprite.svg#heart-20') }}"></use></svg> <span class="indicator__value">0</span></span></a></div>
                            <div class="indicator indicator--trigger--click"><a href="{{ url('cart') }}" class="indicator__button"><span class="indicator__area"><svg width="20px" height="20px"><use xlink:href="{{ asset('assets/images/sprite.svg#cart-20') }}"></use></svg> <span class="indicator__value">3</span></span></a>
                                <div class="indicator__dropdown">
                                    <!-- .dropcart -->
                                    <div class="dropcart">
                                        <div class="dropcart__products-list">
                                            <div class="dropcart__product">
                                                <div class="dropcart__product-image">
                                                    <a href="product.html"><img src="{{asset('assets/images/products/product-1.jpg')}}" alt=""></a>
                                                </div>
                                                <div class="dropcart__product-info">
                                                    <div class="dropcart__product-name"><a href="product.html">Electric Planer Brandix KL370090G 300 Watts</a></div>
                                                    <ul class="dropcart__product-options">
                                                        <li>Color: Yellow</li>
                                                        <li>Material: Aluminium</li>
                                                    </ul>
                                                    <div class="dropcart__product-meta"><span class="dropcart__product-quantity">2</span> x <span class="dropcart__product-price">$699.00</span></div>
                                                </div>
                                                <button type="button" class="dropcart__product-remove btn btn-light btn-sm btn-svg-icon">
                                                    <svg width="10px" height="10px">
                                                        <use xlink:href="{{ asset('assets/images/sprite.svg#cross-10') }}"></use>
                                                    </svg>
                                                </button>
                                            </div>
                                            <div class="dropcart__product">
                                                <div class="dropcart__product-image">
                                                    <a href="product.html"><img src="{{asset('assets/images/products/product-2.jpg')}}" alt=""></a>
                                                </div>
                                                <div class="dropcart__product-info">
                                                    <div class="dropcart__product-name"><a href="product.html">Undefined Tool IRadix DPS3000SY 2700 watts</a></div>
                                                    <div class="dropcart__product-meta"><span class="dropcart__product-quantity">1</span> x <span class="dropcart__product-price">$849.00</span></div>
                                                </div>
                                                <button type="button" class="dropcart__product-remove btn btn-light btn-sm btn-svg-icon">
                                                    <svg width="10px" height="10px">
                                                        <use xlink:href="{{ asset('assets/images/sprite.svg#cross-10') }}"></use>
                                                    </svg>
                                                </button>
                                            </div>
                                            <div class="dropcart__product">
                                                <div class="dropcart__product-image">
                                                    <a href="product.html"><img src="{{asset('assets/images/products/product-5.jpg')}}" alt=""></a>
                                                </div>
                                                <div class="dropcart__product-info">
                                                    <div class="dropcart__product-name"><a href="product.html">Brandix Router Power Tool 2017ERXPK</a></div>
                                                    <ul class="dropcart__product-options">
                                                        <li>Color: True Red</li>
                                                    </ul>
                                                    <div class="dropcart__product-meta"><span class="dropcart__product-quantity">3</span> x <span class="dropcart__product-price">$1,210.00</span></div>
                                                </div>
                                                <button type="button" class="dropcart__product-remove btn btn-light btn-sm btn-svg-icon">
                                                    <svg width="10px" height="10px">
                                                        <use xlink:href="{{ asset('assets/images/sprite.svg#cross-10') }}"></use>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="dropcart__totals">
                                            <table>
                                                <tr>
                                                    <th>Subtotal</th>
                                                    <td>$5,877.00</td>
                                                </tr>
                                                <tr>
                                                    <th>Shipping</th>
                                                    <td>$25.00</td>
                                                </tr>
                                                <tr>
                                                    <th>Tax</th>
                                                    <td>$0.00</td>
                                                </tr>
                                                <tr>
                                                    <th>Total</th>
                                                    <td>$5,902.00</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="dropcart__buttons"><a class="btn btn-secondary" href="{{ url('cart') }}">View Cart</a> <a class="btn btn-primary" href="{{ url('checkout') }}">Checkout</a></div>
                                    </div>
                                    <!-- .dropcart / end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>