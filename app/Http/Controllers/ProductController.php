<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Product;
use App\Models\CategoryProduct;

class ProductController extends Controller
{
    public function index() {

        $content = Page::where('slug', 'products')->first();
        $products = Product::where('status', 1)->orderBy('order', 'asc')->paginate(15);
        $categories = CategoryProduct::where('status', 1)->get();

        return view('products.product-page', compact('content', 'products', 'categories'));
    }

    public function show($slug) {

        $content = Product::where('slug', $slug)->first();

        if (!$content) abort(404);

        $products = Product::where('status', 1)->orderBy('order', 'asc')->get();

        $categories = CategoryProduct::where('status', 1)->get();
        

        return view('products.product', compact('content', 'products', 'categories'));
    }

    public function category($slug) {

        $content = CategoryProduct::where('slug', $slug)->first();

        if (!$content) abort(404);

        $products = Product::where('category_id', $content->id)->where('status', 1)->orderBy('order', 'asc')->paginate(15);
        $popular_products = Product::where('status', 1)->orderByRaw("RAND()")->get();
        $categories = CategoryProduct::where('status', 1)->get();

        return view('products.product-page', compact('content', 'products', 'popular_products', 'categories'));
    }

    public function cart() {

        return view('products.cart');
    }
}
