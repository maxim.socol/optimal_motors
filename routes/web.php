<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index');

Route::get('products', 'ProductController@index')->name('products');
Route::get('/products/{slug}', 'ProductController@show')->name('products');
Route::get('/category/{slug}', 'ProductController@category')->name('category');
Route::get('cart', 'ProductController@cart')->name('cart');

Route::get('about', 'FrontendController@about')->name('about');
Route::get('contact', 'FrontendController@contact')->name('contact');
Route::post('/send-contacts', 'FrontendController@ajaxSendContacts')->name('send-contacts');
Route::get('faq', 'FrontendController@faq')->name('faq');
Route::get('terms', 'FrontendController@terms')->name('terms');

Route::get('account', 'AccountController@account')->name('account');

Route::get('checkout', 'CheckoutController@ckeckout')->name('checkout');

