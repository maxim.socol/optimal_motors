<div class="block-slideshow block-slideshow--layout--with-departments block">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-9 offset-lg-3">
                <div class="block-slideshow__body">
                    <div class="owl-carousel">
                        @forelse ($sliders as $item)     
                            <a class="block-slideshow__slide" href="{{ $item->button_link }}">
                                <div class="block-slideshow__slide-image block-slideshow__slide-image--desktop" style="background-image: url(/storage/{{ $item->photo }})"></div>
                                <div class="block-slideshow__slide-image block-slideshow__slide-image--mobile" style="background-image: url({{asset('assets/images/slides/slide-1-mobile.jpg')}})"></div>
                                <div class="block-slideshow__slide-content">
                                    <div class="block-slideshow__slide-title">
                                        {{ $item->title }}
                                        <br>{{ $item->sub_title }}
                                    </div>
                                    <div class="block-slideshow__slide-text">
                                        {!! $item->description !!}
                                    </div>
                                    <div class="block-slideshow__slide-button"><span class="btn btn-primary btn-lg">{{ $item->button_text }}</span></div>
                                </div>
                            </a>
                        @empty
                            
                        @endforelse
                        <a class="block-slideshow__slide" href="#">
                            <div class="block-slideshow__slide-image block-slideshow__slide-image--desktop" style="background-image: url({{asset('assets/images/slides/slide-2.jpg')}})"></div>
                            <div class="block-slideshow__slide-image block-slideshow__slide-image--mobile" style="background-image: url({{asset('assets/images/slides/slide-2-mobile.jpg')}})"></div>
                            <div class="block-slideshow__slide-content">
                                <div class="block-slideshow__slide-title">Screwdrivers
                                    <br>Professional Tools</div>
                                <div class="block-slideshow__slide-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    <br>Etiam pharetra laoreet dui quis molestie.</div>
                                <div class="block-slideshow__slide-button"><span class="btn btn-primary btn-lg">Shop Now</span></div>
                            </div>
                        </a>
                        <a class="block-slideshow__slide" href="#">
                            <div class="block-slideshow__slide-image block-slideshow__slide-image--desktop" style="background-image: url({{asset('assets/images/slides/slide-3.jpg')}})"></div>
                            <div class="block-slideshow__slide-image block-slideshow__slide-image--mobile" style="background-image: url({{asset('assets/images/slides/slide-3-mobile.jpg')}})"></div>
                            <div class="block-slideshow__slide-content">
                                <div class="block-slideshow__slide-title">One more
                                    <br>Unique header</div>
                                <div class="block-slideshow__slide-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    <br>Etiam pharetra laoreet dui quis molestie.</div>
                                <div class="block-slideshow__slide-button"><span class="btn btn-primary btn-lg">Shop Now</span></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>