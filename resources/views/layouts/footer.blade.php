<footer class="site__footer">
    <div class="site-footer">
        <div class="container">
            <div class="site-footer__widgets">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="site-footer__widget footer-contacts">
                            <h5 class="footer-contacts__title">{{ $configurations->get('contacteaza-ne') }}</h5>
                            <div class="footer-contacts__text">{{ $configurations->get('description-motors') }}
                            </div>
                            <ul class="footer-contacts__contacts">
                                <li><i class="footer-contacts__icon fas fa-globe-americas"></i> Str Intrarea Gheorghe Simionescu nr 4, sector 1, Bucuresti.</li>
                                <li><i class="footer-contacts__icon far fa-envelope"></i> office@optimalmotor.ro</li>
                                <li><i class="footer-contacts__icon fas fa-mobile-alt"></i> 0749130205</li>
                                <li><i class="footer-contacts__icon far fa-clock"></i> Luni - Vineri 9:00 - 20:00</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2">
                        <div class="site-footer__widget footer-links">
                            <h5 class="footer-links__title">Informatii</h5>
                            <ul class="footer-links__list">
                                <li class="footer-links__item"><a href="#" class="footer-links__link">Despre noi</a></li>
                                <li class="footer-links__item"><a href="#" class="footer-links__link">Informatii de livrare</a></li>
                                <li class="footer-links__item"><a href="#" class="footer-links__link">Politica de confidentialitate</a></li>
                                <li class="footer-links__item"><a href="#" class="footer-links__link">Branduri</a></li>
                                <li class="footer-links__item"><a href="#" class="footer-links__link">Contacteaza-ne</a></li>
                                <li class="footer-links__item"><a href="#" class="footer-links__link">Politica de retur</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2">
                        <div class="site-footer__widget footer-links">
                            <h5 class="footer-links__title">Contul meu</h5>
                            <ul class="footer-links__list">
                                <li class="footer-links__item"><a href="#" class="footer-links__link">Locatia noastra</a></li>
                                <li class="footer-links__item"><a href="#" class="footer-links__link">Wish List</a></li>
                                <li class="footer-links__item"><a href="#" class="footer-links__link">Newsletter</a></li>
                                <li class="footer-links__item"><a href="#" class="footer-links__link">Oferte speciale</a></li>
                                <li class="footer-links__item"><a href="#" class="footer-links__link">Gift Certificates</a></li>
                                <li class="footer-links__item"><a href="#" class="footer-links__link">Affiliate</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-4">
                        <div class="site-footer__widget footer-newsletter">
                            <h5 class="footer-newsletter__title">Newsletter</h5>
                            <div class="footer-newsletter__text">Te tinem la curent cu cele mai bune oferte pentru produsele noastre.</div>
                            <form action="#" class="footer-newsletter__form">
                                <label class="sr-only" for="footer-newsletter-address">Email Address</label>
                                <input type="text" class="footer-newsletter__form-input form-control" id="footer-newsletter-address" placeholder="Email Address...">
                                <button class="footer-newsletter__form-button btn btn-primary">Subscribe</button>
                            </form>
                            <div class="footer-newsletter__text footer-newsletter__text--social">Follow us on social networks</div>
                            <ul class="footer-newsletter__social-links">
                                <li class="footer-newsletter__social-link footer-newsletter__social-link--facebook"><a href="https://themeforest.net/user/kos9" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="footer-newsletter__social-link footer-newsletter__social-link--twitter"><a href="https://themeforest.net/user/kos9" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li class="footer-newsletter__social-link footer-newsletter__social-link--youtube"><a href="https://themeforest.net/user/kos9" target="_blank"><i class="fab fa-youtube"></i></a></li>
                                <li class="footer-newsletter__social-link footer-newsletter__social-link--instagram"><a href="https://themeforest.net/user/kos9" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                <li class="footer-newsletter__social-link footer-newsletter__social-link--rss"><a href="https://themeforest.net/user/kos9" target="_blank"><i class="fas fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="site-footer__bottom">
                <div class="site-footer__copyright"><a target="_blank" href="https://www.templateshub.net">Templates Hub</a></div>
                <div class="site-footer__payments"><img src="{{asset('assets/images/payments.png')}}" alt=""></div>
            </div> --}}
        </div>
    </div>
</footer>