<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <title>@if(isset($content)) {{ $content->meta_title }} @endif</title>
    <meta name="description" content="@if(isset($content)) {{ $content->meta_description }} @endif">
    
    @include('layouts.head')
    
</head>

<body>
    <!-- quickview-modal -->
    <div id="quickview-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content"></div>
        </div>
    </div>
    <!-- quickview-modal / end -->
    <!-- mobilemenu -->
    @include('layouts.mobile_menu')
    <!-- mobilemenu / end -->
    <!-- site -->
    <div class="site">
        <!-- mobile site__header -->
        @include('layouts.mobile_header')
        <!-- mobile site__header / end -->
        <!-- desktop site__header -->
        @include('layouts.desktop_header')
        <!-- desktop site__header / end -->
        <!-- site__body -->
        @yield('content')
        <!-- site__body / end -->
        <!-- site__footer -->
        @include('layouts.footer')
        <!-- site__footer / end -->
    </div>
    <!-- site / end -->

    <!-- js -->
    <script src="{{asset('assets/vendor/jquery-3.3.1/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bootstrap-4.2.1/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/vendor/owl-carousel-2.3.4/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/vendor/nouislider-12.1.0/nouislider.min.js')}}"></script>
    <script src="{{asset('assets/js/number.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
    <script src="{{asset('assets/js/script.js')}}"></script>
    <script src="{{asset('assets/vendor/svg4everybody-2.1.9/svg4everybody.min.js')}}"></script>
    <script>
        svg4everybody();
    </script>
    
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-97489509-6"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-97489509-6');
    </script>

    <script>
        $(document).ready(function() {
            $('#contact-form').submit(function(e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "{{ route('send-contacts') }}",
                    data: $(this).serialize(),
                    success: function(data) {
                        $('#contact-form').trigger("reset");
                        alert(data.message);
                        $('.form-messege').text('');
                        $('.form-messege').text('Solicitarea de contact a fost trimisă cu succes');
                    },
                    error: function(data) {

                    },
                });
            });
        });
    </script>

</body>

</html>