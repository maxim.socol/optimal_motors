<div class="block block-products block-products--layout--large-first">
    <div class="container">
        <div class="block-header">
            <h3 class="block-header__title">{{ $configurations->get('cele-mai-vandute') }}</h3>
            <div class="block-header__divider"></div>
        </div>
        <div class="block-products__body">
            <div class="block-products__featured">
                <div class="block-products__featured-item">
                    <div class="product-card">
                        <button class="product-card__quickview" type="button">
                            <svg width="16px" height="16px">
                                <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                            </svg> <span class="fake-svg-icon"></span></button>
                        <div class="product-card__badges-list">
                            <div class="product-card__badge product-card__badge--new">New</div>
                        </div>
                        <div class="product-card__image">
                            <a href="product.html"><img src="{{asset('assets/images/products/Honda_HRX217K6HYA_left.jpg')}}" alt=""></a>
                        </div>
                        <div class="product-card__info">
                            <div class="product-card__name"><a href="product.html">Masina de tuns gazon HRX 537 HYE</a></div>
                            <ul class="product-card__features-list">
                                <li>Speed: 750 RPM</li>
                                <li>Power Source: Cordless-Electric</li>
                                <li>Battery Cell Type: Lithium</li>
                                <li>Voltage: 20 Volts</li>
                                <li>Battery Capacity: 2 Ah</li>
                            </ul>
                        </div>
                        <div class="product-card__actions">
                            <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                            <div class="product-card__prices">$749.00</div>
                            <div class="product-card__buttons">
                                <button class="btn btn-primary product-card__addtocart" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                    <svg width="16px" height="16px">
                                        <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                    </svg> 
                                    <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block-products__list">
                <div class="block-products__list-item">
                    <div class="product-card">
                        <button class="product-card__quickview" type="button">
                            <svg width="16px" height="16px">
                                <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                            </svg> <span class="fake-svg-icon"></span></button>
                        <div class="product-card__badges-list">
                            <div class="product-card__badge product-card__badge--hot">Hot</div>
                        </div>
                        <div class="product-card__image">
                            <a href="product.html"><img src="{{asset('assets/images/products/product-2.jpg')}}" alt=""></a>
                        </div>
                        <div class="product-card__info">
                            <div class="product-card__name"><a href="product.html">Undefined Tool IRadix DPS3000SY 2700 Watts</a></div>
                            <ul class="product-card__features-list">
                                <li>Speed: 750 RPM</li>
                                <li>Power Source: Cordless-Electric</li>
                                <li>Battery Cell Type: Lithium</li>
                                <li>Voltage: 20 Volts</li>
                                <li>Battery Capacity: 2 Ah</li>
                            </ul>
                        </div>
                        <div class="product-card__actions">
                            <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                            <div class="product-card__prices">$1,019.00</div>
                            <div class="product-card__buttons">
                                <button class="btn btn-primary product-card__addtocart" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                    <svg width="16px" height="16px">
                                        <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                    </svg> 
                                    <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products__list-item">
                    <div class="product-card">
                        <button class="product-card__quickview" type="button">
                            <svg width="16px" height="16px">
                                <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                            </svg> <span class="fake-svg-icon"></span></button>
                        <div class="product-card__image">
                            <a href="product.html"><img src="{{asset('assets/images/products/product-3.jpg')}}" alt=""></a>
                        </div>
                        <div class="product-card__info">
                            <div class="product-card__name"><a href="product.html">Drill Screwdriver Brandix ALX7054 200 Watts</a></div>
                            <ul class="product-card__features-list">
                                <li>Speed: 750 RPM</li>
                                <li>Power Source: Cordless-Electric</li>
                                <li>Battery Cell Type: Lithium</li>
                                <li>Voltage: 20 Volts</li>
                                <li>Battery Capacity: 2 Ah</li>
                            </ul>
                        </div>
                        <div class="product-card__actions">
                            <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                            <div class="product-card__prices">$850.00</div>
                            <div class="product-card__buttons">
                                <button class="btn btn-primary product-card__addtocart" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                    <svg width="16px" height="16px">
                                        <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                    </svg> 
                                    <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products__list-item">
                    <div class="product-card">
                        <button class="product-card__quickview" type="button">
                            <svg width="16px" height="16px">
                                <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                            </svg> <span class="fake-svg-icon"></span></button>
                        <div class="product-card__badges-list">
                            <div class="product-card__badge product-card__badge--sale">Sale</div>
                        </div>
                        <div class="product-card__image">
                            <a href="product.html"><img src="{{asset('assets/images/products/product-4.jpg')}}" alt=""></a>
                        </div>
                        <div class="product-card__info">
                            <div class="product-card__name"><a href="product.html">Drill Series 3 Brandix KSR4590PQS 1500 Watts</a></div>
                            <ul class="product-card__features-list">
                                <li>Speed: 750 RPM</li>
                                <li>Power Source: Cordless-Electric</li>
                                <li>Battery Cell Type: Lithium</li>
                                <li>Voltage: 20 Volts</li>
                                <li>Battery Capacity: 2 Ah</li>
                            </ul>
                        </div>
                        <div class="product-card__actions">
                            <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                            <div class="product-card__prices"><span class="product-card__new-price">$949.00</span> <span class="product-card__old-price">$1189.00</span></div>
                            <div class="product-card__buttons">
                                <button class="btn btn-primary product-card__addtocart" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                    <svg width="16px" height="16px">
                                        <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                    </svg> 
                                    <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products__list-item">
                    <div class="product-card">
                        <button class="product-card__quickview" type="button">
                            <svg width="16px" height="16px">
                                <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                            </svg> <span class="fake-svg-icon"></span></button>
                        <div class="product-card__image">
                            <a href="product.html"><img src="{{asset('assets/images/products/product-5.jpg')}}" alt=""></a>
                        </div>
                        <div class="product-card__info">
                            <div class="product-card__name"><a href="product.html">Brandix Router Power Tool 2017ERXPK</a></div>
                            <ul class="product-card__features-list">
                                <li>Speed: 750 RPM</li>
                                <li>Power Source: Cordless-Electric</li>
                                <li>Battery Cell Type: Lithium</li>
                                <li>Voltage: 20 Volts</li>
                                <li>Battery Capacity: 2 Ah</li>
                            </ul>
                        </div>
                        <div class="product-card__actions">
                            <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                            <div class="product-card__prices">$1,700.00</div>
                            <div class="product-card__buttons">
                                <button class="btn btn-primary product-card__addtocart" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                    <svg width="16px" height="16px">
                                        <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                    </svg> 
                                    <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products__list-item">
                    <div class="product-card">
                        <button class="product-card__quickview" type="button">
                            <svg width="16px" height="16px">
                                <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                            </svg> <span class="fake-svg-icon"></span></button>
                        <div class="product-card__image">
                            <a href="product.html"><img src="{{asset('assets/images/products/product-6.jpg')}}" alt=""></a>
                        </div>
                        <div class="product-card__info">
                            <div class="product-card__name"><a href="product.html">Brandix Drilling Machine DM2019KW4 4kW</a></div>
                            <ul class="product-card__features-list">
                                <li>Speed: 750 RPM</li>
                                <li>Power Source: Cordless-Electric</li>
                                <li>Battery Cell Type: Lithium</li>
                                <li>Voltage: 20 Volts</li>
                                <li>Battery Capacity: 2 Ah</li>
                            </ul>
                        </div>
                        <div class="product-card__actions">
                            <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                            <div class="product-card__prices">$3,199.00</div>
                            <div class="product-card__buttons">
                                <button class="btn btn-primary product-card__addtocart" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                    <svg width="16px" height="16px">
                                        <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                    </svg> 
                                    <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products__list-item">
                    <div class="product-card">
                        <button class="product-card__quickview" type="button">
                            <svg width="16px" height="16px">
                                <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                            </svg> <span class="fake-svg-icon"></span></button>
                        <div class="product-card__image">
                            <a href="product.html"><img src="{{asset('assets/images/products/product-7.jpg')}}" alt=""></a>
                        </div>
                        <div class="product-card__info">
                            <div class="product-card__name"><a href="product.html">Brandix Pliers</a></div>
                            <ul class="product-card__features-list">
                                <li>Speed: 750 RPM</li>
                                <li>Power Source: Cordless-Electric</li>
                                <li>Battery Cell Type: Lithium</li>
                                <li>Voltage: 20 Volts</li>
                                <li>Battery Capacity: 2 Ah</li>
                            </ul>
                        </div>
                        <div class="product-card__actions">
                            <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                            <div class="product-card__prices">$24.00</div>
                            <div class="product-card__buttons">
                                <button class="btn btn-primary product-card__addtocart" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                    <svg width="16px" height="16px">
                                        <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                    </svg> 
                                    <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>