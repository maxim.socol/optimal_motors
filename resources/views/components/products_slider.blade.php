<div class="block block-products-carousel" data-layout="grid-4">
    <div class="container">
        <div class="block-header">
            <h3 class="block-header__title">{{ $configurations->get('produse-recomandate') }}</h3>
            <div class="block-header__divider"></div>
            <div class="block-header__arrows-list">
                <button class="block-header__arrow block-header__arrow--left" type="button">
                    <svg width="7px" height="11px">
                        <use xlink:href="{{ asset('assets/images/sprite.svg#arrow-rounded-left-7x11') }}"></use>
                    </svg>
                </button>
                <button class="block-header__arrow block-header__arrow--right" type="button">
                    <svg width="7px" height="11px">
                        <use xlink:href="{{ asset('assets/images/sprite.svg#arrow-rounded-right-7x11') }}"></use>
                    </svg>
                </button>
            </div>
        </div>
        <div class="block-products-carousel__slider">
            <div class="block-products-carousel__preloader"></div>
            <div class="owl-carousel">
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__badges-list">
                                <div class="product-card__badge product-card__badge--new">New</div>
                            </div>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/generator-honda-eu-22-it.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Grup electrogen EU 22 iT</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$749.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__badges-list">
                                <div class="product-card__badge product-card__badge--hot">Hot</div>
                            </div>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/HW-220-1000x1000.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Grup electrogen HW 220</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$1,019.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/honda-eu30ik1.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Grup electrogen EU30 iK1</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$850.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__badges-list">
                                <div class="product-card__badge product-card__badge--sale">Sale</div>
                            </div>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/grebla-gazon.png')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Motosapa FG 201</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices"><span class="product-card__new-price">$949.00</span> <span class="product-card__old-price">$1189.00</span></div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/hrx-537-vkea.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Masina de tuns gazon HRX 537 VKE</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$1,700.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-6.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Brandix Drilling Machine DM2019KW4 4kW</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$3,199.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-7.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Brandix Pliers</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$24.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">{{ $configurations->get('add-to-cart') }}</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-8.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Water Hose 40cm</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$15.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-9.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Spanner Wrench</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$19.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-10.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Water Tap</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$15.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-11.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Hand Tool Kit</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$149.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-12.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Ash's Chainsaw 3.5kW</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$666.99</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-13.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Brandix Angle Grinder KZX3890PQW</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$649.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-14.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Brandix Air Compressor DELTAKX500</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$1,800.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-15.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Brandix Electric Jigsaw JIG7000BQ</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$290.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-products-carousel__column">
                    <div class="block-products-carousel__cell">
                        <div class="product-card">
                            <button class="product-card__quickview" type="button">
                                <svg width="16px" height="16px">
                                    <use xlink:href="{{ asset('assets/images/sprite.svg#quickview-16') }}"></use>
                                </svg> <span class="fake-svg-icon"></span></button>
                            <div class="product-card__image">
                                <a href="product.html"><img src="{{asset('assets/images/products/product-16.jpg')}}" alt=""></a>
                            </div>
                            <div class="product-card__info">
                                <div class="product-card__name"><a href="product.html">Brandix Screwdriver SCREW1500ACC</a></div>
                                <ul class="product-card__features-list">
                                    <li>Speed: 750 RPM</li>
                                    <li>Power Source: Cordless-Electric</li>
                                    <li>Battery Cell Type: Lithium</li>
                                    <li>Voltage: 20 Volts</li>
                                    <li>Battery Capacity: 2 Ah</li>
                                </ul>
                            </div>
                            <div class="product-card__actions">
                                <div class="product-card__availability">Availability: <span class="text-success">In Stock</span></div>
                                <div class="product-card__prices">$1,499.00</div>
                                <div class="product-card__buttons">
                                    <button class="btn btn-primary product-card__addtocart" type="button">Add To Cart</button>
                                    <button class="btn btn-secondary product-card__addtocart product-card__addtocart--list" type="button">Add To Cart</button>
                                    <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">
                                        <svg width="16px" height="16px">
                                            <use xlink:href="{{ asset('assets/images/sprite.svg#wishlist-16') }}"></use>
                                        </svg> 
                                        <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>